public class Buch implements Comparable<Buch> {
	private String autor, titel, isbn;

	public Buch(String autor, String titel, String isbn) {
		super();
		this.autor = autor;
		this.titel = titel;
		this.isbn = isbn;
	}
	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public boolean equals(Buch b) {
		return isbn.equals(b.isbn);
	}

	@Override
	public int compareTo(Buch o) {
		return isbn.compareTo(o.isbn);
	}
	@Override
	public String toString() {
		return "Buch [autor=" + autor + ", titel=" + titel + ", isbn=" + isbn + "]";
	}
}
