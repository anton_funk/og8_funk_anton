import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class DatabaseConection {
	private String driver="com.mysql.jdbc.Driver";
	private String url="jdbc:mysql://localhost/keks";
	private String user="root";
	private String passwort="";
	@SuppressWarnings("unused")
	public boolean insertKekse(String bezeichnung, String sorte) {
		String sql="INSERT INTO T_Kekse(bezeichnung, sorte) Values('"+bezeichnung+"','"+sorte+"')";
		try {
			// jdbc treiber laden
			Class.forName(driver);
			// verbindung aufbauen
			Connection con= DriverManager.getConnection(url,user,passwort);
			// statement erstellen
			Statement stmt=con.createStatement();
			
			stmt.executeUpdate(sql);

			// verbindung beenden
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public static void main(String[] args) {
		String[]standatrsorten= {
				"Schoko",
				"Hafer",
				"Kokos",
				"null"
		};
		DatabaseConection dbc=new DatabaseConection();
		for(int i=0;i<100;i++) {
		dbc.insertKekse("Standartkeks"+i, standatrsorten[i%standatrsorten.length]);
		}
	}
}
