
public abstract class Person {
	String name;
	long telefonnummer;
	boolean jahresbeitragBezahlt;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getTelefonnummer() {
		return telefonnummer;
	}
	public void setTelefonnummer(long telefonnummer) {
		this.telefonnummer = telefonnummer;
	}
	public boolean isJahresbeitragBezahlt() {
		return jahresbeitragBezahlt;
	}
	public void setJahresbeitragBezahlt(boolean jahresbeitragBezahlt) {
		this.jahresbeitragBezahlt = jahresbeitragBezahlt;
	}
	public Person(String name, long telefonnummer) {
		this.name = name;
		this.telefonnummer = telefonnummer;
		jahresbeitragBezahlt=false;
	}
	
	
	
}
