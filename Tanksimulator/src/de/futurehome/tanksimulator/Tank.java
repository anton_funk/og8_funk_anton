package de.futurehome.tanksimulator;
public class Tank {
	
	private double fuellstand;

	public Tank(double fuellstand) {
		this.fuellstand = fuellstand;
	}

	public double getFuellstand() {
		return fuellstand;
	}

	public void setFuellstand(double fuellstand) {
		this.fuellstand = Math.max(fuellstand,0);
	}

}
