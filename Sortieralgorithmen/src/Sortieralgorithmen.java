import java.util.Arrays;

public abstract class Sortieralgorithmen {
	public static int[] boublesort(int[]a) {
		int c=0;
		for(int i=a.length;i>1;i--) {
			boolean madeChange=false;
			for(int j=0;j<i-1;j++) {
				if(a[j]>a[j+1]) {
					int temp=a[j];
					a[j]=a[j+1];
					a[j+1]=temp;
					madeChange=true;
				}
				c++;
			}
			if(!madeChange)
				break;
		}
		System.out.println("Vergleiche: "+c);//+"\t"+Arrays.toString(a));
		return a;
	}
	public static int[] mergesort(int[]a) {
		if(a.length<=1)
			return a;
		//System.out.println(Arrays.toString(a));
		a=merge(mergesort(upperHalf(a)),mergesort(lowerHalf(a)));
		//System.out.println(Arrays.toString(a));
		return a;
	}
	private static int[] lowerHalf(int[]a) {
		int[]b=new int[a.length/2];
		for(int i=0;i<b.length;i++) {
			b[i]=a[i];
		}
		return b;
	}
	private static int[] upperHalf(int[]a) {
		int[]b=new int[(a.length+1)/2];
		for(int i=0;i<b.length;i++) {
			b[i]=a[(a.length-1)-i];
		}
		return b;
	}
	public static int[] merge(int[]a,int[]b) {
		System.out.println(Arrays.toString(a)+" + "+Arrays.toString(b));
		int[]c=new int[a.length+b.length];
		for(int i=0,j=0;i<a.length||j<b.length;) {
			if(i<a.length&&j<b.length) {
				if(a[i]<b[j]) {
					c[i+j]=a[i];
					i++;
				}else {
					c[i+j]=b[j];
					j++;
				}
			}else if(j==b.length){
				c[i+j]=a[i];
				i++;
			}else {
				c[i+j]=b[j];
				j++;
			}
				
		}
		System.out.println('='+Arrays.toString(c)+'\n');
		return c;
	}
}
