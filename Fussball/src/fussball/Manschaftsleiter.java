package fussball;

public class Manschaftsleiter extends Spieler {
	private int beitragsrabatt;		//in prozent
	
	public Manschaftsleiter(String name, long telefonnummer, int trikotnummer, String position, Mannschaft mannschaft,
			int beitragsrabatt) {
		super(name, telefonnummer, trikotnummer, position, mannschaft);
		this.beitragsrabatt = beitragsrabatt;
	}
	public int getBeitragsrabatt() {
		return beitragsrabatt;
	}
	public void setBeitragsrabatt(int beitragsrabatt) {
		this.beitragsrabatt = beitragsrabatt;
	}

}
