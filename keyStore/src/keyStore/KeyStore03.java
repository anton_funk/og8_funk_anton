package keyStore;

import java.util.LinkedList;

public class KeyStore03 {
	private Liste[] content;
	public KeyStore03() {
		content=new Liste[10];
		for(int i=0;i<content.length;i++)
			content[i]=new Liste();
		
	}
	public KeyStore03(int length) {
		content=new Liste[length];
		for(int i=0;i<content.length;i++)
			content[i]=new Liste();
	}
	public boolean 	add(String s) {
		return content[hash(s)].add(s);
	}
	public void clear() {
		for(Liste l:content)
		l.clear();
	}
	public String get(int i) {
		for(int j=0;j<content.length;j++) {
			if(i-content[j].size()>=0)
			i-=content[j].size();
			else
				return content[j].get(i);
		}
		return null;
		
	}
	public int indexOf(String s){
		int i=0,j;
		for(j=0;j<hash(s);j++) {
			i+=content[j].size();
		}
		return i+content[j].indexOf(s);
	}
	String remove(int i) {
		for(int j=0;j<content.length;j++) {
			if(i-content[j].size()>=0)
			i-=content[j].size();
			else
				return content[j].remove(i);
		}
		return null;
	}
	boolean remove(String s) {
		return content[hash(s)].remove(s);
	}
	int size(){
		int size=0;
		for(Liste l:content)
			size+=l.size();
		return size;
	}
	
	@Override
	public String toString() {
		String s="[";
		for(Liste l:content)
			s+=l.toString();
		return s+']';
	}
	private int hash(String s) {
		return s.hashCode()%content.length;
	}
	class Liste extends LinkedList<String>{
		private static final long serialVersionUID = 1L;
		
	}
}
