package fussball;

public class Schiedsrichter extends Vereinsmitglied {
	private int anzahlSpiele;

	public Schiedsrichter(String name, long telefonnummer) {
		super(name, telefonnummer);
		this.anzahlSpiele = 0;
	}

	public int getAnzahlSpiele() {
		return anzahlSpiele;
	}

	public void setAnzahlSpiele(int anzahlSpiele) {
		this.anzahlSpiele = anzahlSpiele;
	}
}
