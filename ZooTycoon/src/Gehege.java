
public class Gehege {
	private double flaeche;
	private int preis,attraktivitaet;
	private Affe[]affen;
	public Affe[] getListAffen() {
		return affen;
	}
	public void setListAffen(Affe[] affen) {
		this.affen = affen;
	}
	public double getFlaeche() {
		return flaeche;
	}
	public void setFlaeche(double flaeche) {
		this.flaeche = flaeche;
	}
	public int getPreis() {
		return preis;
	}
	public void setPreis(int preis) {
		this.preis = preis;
	}
	public int getAttraktivitaet() {
		return attraktivitaet;
	}
	public void setAttraktivitaet(int atraktivitaet) {
		this.attraktivitaet = atraktivitaet;
	}

}
