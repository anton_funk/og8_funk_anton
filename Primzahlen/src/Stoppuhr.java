
public class Stoppuhr {
	private long startzeit,endzeit;
	public void start() {
		startzeit=System.currentTimeMillis();
	}
	public void stopp() {
		endzeit=System.currentTimeMillis();
	}
	public void reset() {
		//ist nutzlos muss aber wegen ihren Vorgaben rein
		startzeit=0;
		endzeit=0;
	}
	public long getDauerInMs() {
		return endzeit-startzeit;
	}
}
