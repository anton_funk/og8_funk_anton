
public class Spieler extends Person {
	int trikonummer;
	String position;
	
	public Spieler(String name, long telefonnummer) {
		super(name, telefonnummer);
		
	}
	public int getTrikonummer() {
		return trikonummer;
	}
	public void setTrikonummer(int trikonummer) {
		this.trikonummer = trikonummer;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}

}
