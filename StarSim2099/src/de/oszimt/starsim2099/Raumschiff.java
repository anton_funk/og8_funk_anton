package de.oszimt.starsim2099;

/**
 * Write a description of class Raumschiff here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Raumschiff extends PositionedObject{
	private String typ;
	  private int maxKapazitaet;
	  private String antrieb;
	  private int winkel;
	  
	  public void setTyp(String typNeu) {
	    typ = typNeu;
	  }

	  public String getTyp() {
	    return typ;
	  }

	  public void setMaxLadekapazitaet(int maxKapazitaetNeu) {
	    maxKapazitaet = maxKapazitaetNeu;
	  }

	  public int getMaxLadekapazitaet() {
	    return maxKapazitaet;
	  }


	  public String getAntrieb() {
	    return antrieb;
	  }

	  public void setAntrieb(String antriebNeu) {
	    antrieb = antriebNeu;
	  }

	  public int getWinkel() {
	    return winkel;
	  }

	  public void setWinkel(int winkelNeu) {
	    winkel = winkelNeu;
	  }
	// Darstellung
	public static char[][] getDarstellung() {
		char[][] raumschiffShape = { 
				{'\0', '\0','_', '\0', '\0'},
				{'\0', '/', 'X', '\\', '\0'},
				{'\0', '{', 'X', '}', '\0'},
				{'\0', '{', 'X', '}', '\0'},
				{'/', '_', '_','_', '\\'},				
		};
		return raumschiffShape;
	}

}
