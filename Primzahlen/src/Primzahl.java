
public abstract class Primzahl {
	static boolean isPrimzahl(long l) {
		if(l<2)return false;
		for(long i=2;i<l;i++) {
			if(l%i==0)return false;
		}
		return true;
	}
}
