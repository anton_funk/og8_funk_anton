public class TestGehege {
	public static void main(String args) {
		// Erstellen des Objektes
		Gehege meinGehege= new Gehege();
		// Setzen der Werte f�r das Gehege
		double flaeche=23;
		int attraktivitaet=0;
		meinGehege.setFlaeche(flaeche);
		meinGehege.setPreis(30000);
		meinGehege.setAttraktivitaet(attraktivitaet);
		// Meine Affen
		Affe a1= new Affe("Bert");
		Affe a2= new Affe("Berta");
		Affe[] meineAffen= new Affe[10];
		meineAffen[0] = a1;
		meineAffen[1] = a2;
		// Alter setzen
		meineAffen[0].setAlter(14);
		meineAffen[2].setAlter(12);
		// Affen ins Gehege setzen
		meinGehege.setListAffen(meineAffen);
		// Berta bricht aus!
		a2.setGehege(null);
		// Schaue nach, ob Berta im richtigen Gehege ist
		if(a2.getGehege() == meinGehege) {
			System.out.println("Berta ist im Gehege.");
		} else {
			System.out.println("Berta ist irgendwo anders!");
		}// Wer ist denn so im Gehege?"
		for(int i = 0;i<=meinGehege.getListAffen().length;i++){
			if(meinGehege.getListAffen()[i] != null) {
				System.out.println("Affe Nr. "+ i + 1 + ": "+meinGehege.getListAffen()[i].getName());
			}
		}
	}
}
