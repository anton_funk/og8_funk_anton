package keyStore;

public class KeyStore01{
	private String[] content;
	public KeyStore01() {
		content=new String[100];
	}
	public KeyStore01(int length) {
		content=new String[length];
	}
	public boolean 	add(String e) {
		for(int i=0;i<content.length;i++) {
			if(content[i]==null) {
				content[i]=e;
				return true;
			}
		}
		String[] contentNew=new String[content.length+1];
		for(int i=0;i<content.length;i++) {
			contentNew[i]=content[i];
		}
		content=contentNew;
		return false;
	}
	public void clear() {
		content=new String[5];
	}
	public String get(int i) {
		if(i>=0&&i<content.length)
			return content[i];
		return null;
		
	}
	public int indexOf(String s){
		for(int i=0;i<content.length;i++)
			if(s.equals(content[i]))
				return i;
		return -1;
	}
	boolean remove(int i) {
		if(i<0&&i>=content.length)
			return false;
		content[i]=null;
		moveToFront();
		return true;
	}
	boolean remove(String s) {
		int i=indexOf(s);
		if(i==-1)return false;
		content[i]=null;
		moveToFront();
		return true;
	}
	int size(){
		return content.length;
	}
	
	@Override
	public String toString() {
		String s="KeyStore01 [";
		for(int i=0;i<content.length;i++) {
			if(content[i]!=null)
				s+=(i==0?"":", ")+content[i];

		}
		return s+']';
	}

	private void moveToFront() {
		for(int i=0;i<content.length;i++) {
			if(content[i]==null) {
				for(int j=i;j<content.length;j++) {
					if(content[i]!=null) {
						content[i]=content[j];
						content[j]=null;
						break;
					}
				}
			}
		}
	}
}
