import java.util.ArrayList;
import java.util.Random;

public class Account {
	String name, salt, hash;
	ArrayList<Reposetory> reposetorys;

	public Account(String name,String passwort) {
		reposetorys = new ArrayList<Reposetory>();
		this.name = name;
		salt="";
		Random rand=new Random();
		for(int i=0;i<100;i++)
			salt+=(char)rand.nextInt();
		hash=sichererHash(passwort+salt);
	}

	public String getName() {
		return name;
	}

	public String getSalt() {
		return salt;
	}

	public String getHash() {
		return hash;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Reposetory> getReposetorys() {
		return reposetorys;
	}

	public void setReposetorys(ArrayList<Reposetory> reposetorys) {
		this.reposetorys = reposetorys;
	}

	public void addReposetory(Reposetory reposetory) {
		reposetorys.remove(reposetory);
	}

	public void removeReposetory(Reposetory reposetory) {
		reposetorys.remove(reposetory);
	}

	boolean testPasswort(String zuTesten) {
		return zuTesten.equals(sichererHash(zuTesten + salt));
	}

	void aenderePasswort(String passwortAlt, String passwortNeu) {
		if (testPasswort(passwortAlt))
			hash = sichererHash(passwortAlt + salt);
	}

	static String sichererHash(String toHash) {
		return "" + toHash.hashCode();
	}
}
