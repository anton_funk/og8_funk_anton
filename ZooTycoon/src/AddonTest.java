import java.util.Scanner;

public class AddonTest {

	public static void main(String[] args) {
		Scanner scan=new Scanner(System.in);
		Addon addon1=new Addon("Geldwaschanlage",290,1,2);
		Addon addon2=new Addon("Vogelspinnenfutter",49,15,100);
		System.out.println(addon1.getName()+" hat die id "+addon1.getId_nummer());
		System.out.println(addon2.getName()+" hat die id "+addon2.getId_nummer()+" auf lager: "+addon2.getAnzahl());
		if(addon2.kaufen(scan.nextInt()))
			System.out.println("Kauf erfolgreich");
		else
			System.out.println("Kauf fehlgeschlagen");
		System.out.println(addon2.getName()+" hat die id "+addon2.getId_nummer()+" auf lager: "+addon2.getAnzahl());
		scan.close();
	}

}
