import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class KindGenerator {
	public static List<Kind> generateKinderListe() {
		List<Kind> list = new ArrayList<Kind>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File("kinddaten.txt")));
			while (br.ready())
				list.add(Kind.generateFromText(br.readLine()));
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
}