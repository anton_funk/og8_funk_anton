import java.io.PrintStream;

public class PrimzahlTestLaufzeit {

	public static void main(String[] args) {
		long max=1_000_000l;
		try {
		PrintStream out= new PrintStream("C:\\Users\\user\\Documents\\Laufzeiten.txt");
		Stoppuhr uhr= new Stoppuhr();
		for(long i=9l;i<max;i+=10) {
			uhr.start();
			boolean isprim=Primzahl.isPrimzahl(i);
			uhr.stopp();
			if(isprim) {
				out.println(i+"\t"+uhr.getDauerInMs());
			}
			out.flush();
			out.close();
		}
		}catch(Exception e) {
			System.out.println(e);
		}
		
		System.out.println("fertig");
	}

}
