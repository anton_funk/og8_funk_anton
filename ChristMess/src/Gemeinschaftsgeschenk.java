import java.util.ArrayList;
import java.util.List;

public class Gemeinschaftsgeschenk extends Geschenk {
	List<Kind> kinder;
	public Gemeinschaftsgeschenk(String bezeichnung, int preis, Kind kind) {
		super(bezeichnung, preis, kind);
		kinder=new ArrayList<Kind>();
		kinder.add(kind);
	}
	public Gemeinschaftsgeschenk(String bezeichnung, int preis, List<Kind> kinder) {
		super(bezeichnung, preis, kinder.get(0));
		this.kinder=kinder;
	}
	public void addKind(Kind k) {
		kinder.add(k);
	}
	public List<Kind> getKinder() {
		return kinder;
	}
	public boolean fuerKind(Kind k) {
		for(Kind kind:kinder)
			if(k.equals(kind))
				return true;
		return false;
	}
}
