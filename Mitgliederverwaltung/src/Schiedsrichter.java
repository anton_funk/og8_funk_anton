
public class Schiedsrichter extends Person {
	private int anzahlSpiele;
	public Schiedsrichter(String name, long telefonnummer) {
		super(name, telefonnummer);
		setAnzahlSpiele(0);
	}
	public int getAnzahlSpiele() {
		return anzahlSpiele;
	}
	public void setAnzahlSpiele(int anzahlSpiele) {
		this.anzahlSpiele = anzahlSpiele;
	}

}
