
public class Geschenk {
	private String bezeichnung;
	private int preis;
	private Kind kind;
	public Geschenk(String bezeichnung, int preis, Kind kind) {
		super();
		this.bezeichnung = bezeichnung;
		this.preis = preis;
		this.kind = kind;
	}
	public String getBezeichnung() {
		return bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	public int getPreis() {
		return preis;
	}
	public void setPreis(int preis) {
		this.preis = preis;
	}
	public Kind getKind() {
		return kind;
	}
	public void setKind(Kind kind) {
		this.kind = kind;
	}
	public boolean fuerKind(Kind k) {
		return k.equals(kind);
	}
}
