package fussball;

import java.util.ArrayList;

public class Trainer extends Vereinsmitglied {
	private char lizensklasse;
	private int aufwandsentschaedigung;
	private ArrayList<Mannschaft> mannschaften;
	private Spielklasse spielklasse;
	public Trainer(String name, long telefonnummer, char lizensklasse, int aufwandsentschaedigung,
			ArrayList<Mannschaft> mannschaften, Spielklasse spielklasse) {
		super(name, telefonnummer);
		this.lizensklasse = lizensklasse;
		this.aufwandsentschaedigung = aufwandsentschaedigung;
		this.mannschaften = mannschaften;
		this.spielklasse = spielklasse;
	}
	public char getLizensklasse() {
		return lizensklasse;
	}
	public void setLizensklasse(char lizensklasse) {
		this.lizensklasse = lizensklasse;
	}
	public int getAufwandsentschaedigung() {
		return aufwandsentschaedigung;
	}
	public void setAufwandsentschaedigung(int aufwandsentschaedigung) {
		this.aufwandsentschaedigung = aufwandsentschaedigung;
	}
	public ArrayList<Mannschaft> getMannschaften() {
		return mannschaften;
	}
	public void setMannschaften(ArrayList<Mannschaft> mannschaften) {
		this.mannschaften = mannschaften;
	}
	public Spielklasse getSpielklasse() {
		return spielklasse;
	}
	public void setSpielklasse(Spielklasse spielklasse) {
		this.spielklasse = spielklasse;
	}
}
