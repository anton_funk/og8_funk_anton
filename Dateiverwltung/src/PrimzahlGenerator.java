
public class PrimzahlGenerator {
	long iterator=0;
	
	public long nextPrim() {
		
		do;while(!isPrim(++iterator));
		return iterator;
	}
	
	private boolean isPrim(long p){
		long sqrt=(long) (Math.sqrt(p)+1);
		if(p<2)return false;
		for(long i=2;i<sqrt;i++) {
			if(p%i==0) {
				return false;
			}
		}
		return true;
	}

	public void setIterator(long iterator) {
		this.iterator = iterator;
	}
}
