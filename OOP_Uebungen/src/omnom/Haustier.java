package omnom;

public class Haustier {
	private int hunger,muede,zufrieden,gesund;
	private String name;
	public Haustier() {
		this.hunger=100;
		this.muede=100;
		this.zufrieden=100;
		this.gesund=100;
	}
	public Haustier(String name) {
		this.name = name;
		this.hunger=100;
		this.muede=100;
		this.zufrieden=100;
		this.gesund=100;
	}
	public int getHunger() {
		return hunger;
	}
	public void setHunger(int hunger) {
		
		this.hunger = limit(hunger,0,100);
	}
	public int getMuede() {
		return muede;
	}
	public void setMuede(int muede) {
		this.muede = limit(muede,0,100);
	}
	public int getZufrieden() {
		return zufrieden;
	}
	public void setZufrieden(int zufrieden) {
		this.zufrieden = limit(zufrieden,0,100);
	}
	public int getGesund() {
		return gesund;
	}
	public void setGesund(int gesund) {
		this.gesund = limit(gesund,0,100);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void fuettern(int i) {
		setHunger(hunger+i);
	}
	public void schlafen(int i) {
		setMuede(muede+i);
	}
	public void spielen(int i) {
		setZufrieden(zufrieden+i);
	}
	public void heilen() {
		setGesund(100);
	}
	public static int limit(int value,int lowLimit,int hightLimit) {
		return Math.max(lowLimit,Math.min(hightLimit,value));
	}
}
