package de.oszimt.starsim2099;

/**
 * Write a description of class Ladung here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Ladung extends PositionedObject{

	private String typ;
	  private int masse;
	  // Ende Attribute
	  // Anfang Methoden
	  public void setTyp(String typNeu) {
	    typ = typNeu;
	  }

	  public String getTyp() {
	    return typ;
	  }

	  public void setMasse(int masseNeu) {
	    masse = masseNeu;
	  }

	  public int getMasse() {
	    return masse;
	  }

	  
	// Darstellung
	public static char[][] getDarstellung() {
		char[][] ladungShape = { { '/', 'X', '\\' }, { '|', 'X', '|' }, { '\\', 'X', '/' } };
		return ladungShape;
	}
}