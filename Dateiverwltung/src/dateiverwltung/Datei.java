package dateiverwltung;
import java.io.*;
import java.util.Scanner;
public class Datei {
	private File file;
	public Datei(File file) {
		super();
		this.file = file;
	}
	public void write(String s) {
		try {
			BufferedWriter bw=new BufferedWriter(new FileWriter(file,true));
			bw.write(s+'\n');
			bw.flush();
			bw.close();
		} catch (IOException e) {
			System.out.println("File writing problem");
			e.printStackTrace();
		}
	}
	public int lastNumber() {
		try {
		Scanner scan;
		scan = new Scanner(file);
		int last=0;
		while(scan.hasNextLine())
		last= scan.nextInt();
		return last;
		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return 0;
	}
	public String lese() {
		try {
			BufferedReader br=new BufferedReader(new FileReader(file));
			String s="";
			while(br.ready())s+=br.readLine();
			return s;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} return null;
	}
	public void print(String s) {
		try {
			PrintStream ps= new PrintStream(file);
			ps.println(s);
			ps.flush();
			ps.close();
		} catch (IOException e) {
			System.out.println("File writing problem");
			e.printStackTrace();
		}
	}
}
