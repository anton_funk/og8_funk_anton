package fussball;

public class Mannschaft {
	private String name;
	private Spielklasse spielklasse;
	private Spieler[]spieler=new Spieler[22];
	public Mannschaft(String name, Spielklasse spielklasse, Spieler[] spieler) {
		super();
		this.name = name;
		this.spielklasse = spielklasse;
		this.setSpieler(spieler);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Spielklasse getSpielklasse() {
		return spielklasse;
	}
	public void setSpielklasse(Spielklasse spielklasse) {
		this.spielklasse = spielklasse;
	}
	public Spieler[] getSpieler() {
		return spieler;
	}
	public void setSpieler(Spieler[] spieler) {
		if(spieler!=null) 
			if(spieler.length>=11&&spieler.length<=22){
				this.spieler = spieler;
				for(Spieler s:spieler)
					if(s!=null)
						s.setMannschaft(this);
			}
	}
}