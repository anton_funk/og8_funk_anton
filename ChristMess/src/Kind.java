public class Kind implements Comparable<Kind> {
	private String vorname, nachname, geburtsdatum, ort;
	private int bravheitsgrad;

	public Kind(String vorname, String nachname, String geburtsdatum, int bravheitsgrad, String ort) {
		super();
		this.vorname = vorname;
		this.nachname = nachname;
		this.geburtsdatum = geburtsdatum;
		this.ort = ort;
		this.bravheitsgrad = bravheitsgrad;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public void setGeburtsdatum(String geburtsdatum) {
		this.geburtsdatum = geburtsdatum;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}

	public int getBravheitsgrad() {
		return bravheitsgrad;
	}

	public void setBravheitsgrad(int bravheitsgrad) {
		this.bravheitsgrad = bravheitsgrad;
	}

	@Override
	public String toString() {
		return "[" + vorname + ", " + nachname + ", " + geburtsdatum + ", " + bravheitsgrad + ", " + ort + "]";
	}

	public static Kind generateFromText(String s) {
		try {
			s = s.replaceAll(",", "");
			String[] a = s.split(" ");
			for (int i = 5; i < a.length; i++)
				a[4] += ' '+a[i];
			return new Kind(a[0], a[1], a[2], Integer.parseInt(a[3].trim()), a[4]);

		} catch (Exception e) {
			System.err.println("konnte \""+s+"\" nicht zu Kind umwandeln");
			//e.printStackTrace();
		}
		return null;
	}

	@Override
	public int compareTo(Kind k) {
		if (bravheitsgrad != k.getBravheitsgrad())
			return k.getBravheitsgrad() - bravheitsgrad;
		return ort.compareTo(k.getOrt());
	}

}
