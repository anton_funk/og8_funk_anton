
public class Addon {
	private static int nextId = 0;
	private String name;
	private int id_nummer;
	private int preis;
	private int anzahl;
	private int maxAnzahl;

	
	public Addon(String name, int preis, int anzahl, int maxAnzahl) {
		this.setName(name);
		this.setId_nummer(Addon.getNextId());
		
		this.setPreis(preis);
		this.setAnzahl(anzahl);
		this.setMaxAnzahl(maxAnzahl);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static int getNextId() {
		return nextId++;
	}

	public int getId_nummer() {
		return id_nummer;
	}

	public void setId_nummer(int id_nummer) {
		this.id_nummer = id_nummer;
	}

	public int getPreis() {
		return preis;
	}

	public void setPreis(int preis) {
		this.preis = preis;
	}

	public int getAnzahl() {
		return anzahl;
	}

	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}

	public int getMaxAnzahl() {
		return maxAnzahl;
	}

	public void setMaxAnzahl(int maxAnzahl) {
		this.maxAnzahl = maxAnzahl;
	}
	public static int berechneGesammtWert(Addon[]addons) {
		int ges=0;
		for(Addon a:addons) {
			ges+=a.getPreis();
		}
		return ges;
	}
	boolean kaufen(int menge) {
		if(anzahl+menge<=maxAnzahl) {
			anzahl+=menge;
			return true;
		}
		return false;
	}
	boolean verbrauchen(int menge) {
		if(anzahl-menge>=0) {
			anzahl-=menge;
			return true;
		}
		return false;
	}
	
}
