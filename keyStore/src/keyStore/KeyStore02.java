package keyStore;

import java.util.ArrayList;

public class KeyStore02{
	
	private ArrayList<String> content;
	public KeyStore02() {
		content=new ArrayList<String>();
	}
	public KeyStore02(int length) {
		content=new ArrayList<String>(length);
	}
	public boolean 	add(String e) {
		return content.add(e);
	}
	public void clear() {
		content.clear();
	}
	public String get(int i) {
		return content.get(i);
		
	}
	public int indexOf(String s){
		return content.indexOf(s);
	}
	boolean remove(int i) {
		return content.remove(i) != null;
	}
	boolean remove(String s) {
		return content.remove(s);
	}
	int size(){
		return content.size();
	}
	
	@Override
	public String toString() {
		return content.toString();
	}
}
