package fussball;

public class Spieler extends Vereinsmitglied {
	private int trikotnummer;
	private String position;
	private Mannschaft mannschaft;
	public Spieler(String name, long telefonnummer, int trikotnummer, String position, Mannschaft mannschaft) {
		super(name, telefonnummer);
		this.trikotnummer = trikotnummer;
		this.position = position;
		this.mannschaft = mannschaft;
	}
	public int getTrikotnummer() {
		return trikotnummer;
	}
	public void setTrikotnummer(int trikotnummer) {
		this.trikotnummer = trikotnummer;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public Mannschaft getMannschaft() {
		return mannschaft;
	}
	public void setMannschaft(Mannschaft mannschaft) {
		this.mannschaft = mannschaft;
	}
	
}
