import java.util.ArrayList;
import java.util.List;

public class Sack {
	private List <Geschenk> geschenke= new ArrayList <Geschenk>();
	private static Geschenk rute=new Gemeinschaftsgeschenk("rute",0,(Kind)null);
	public Sack(List <Geschenk>geschenke) {
		this.geschenke=geschenke;
	}
	public Sack(Geschenk geschenk) {
		geschenke.add(geschenk);
	}
	public List<Geschenk> getGeschenke(Kind k){
		List<Geschenk> fuerKind= new ArrayList<Geschenk>();
		if(k.getBravheitsgrad()>0) {
			for(Geschenk g:geschenke) {
				if(g.fuerKind(k))
					fuerKind.add(g);
			}
		}
		else
			fuerKind.add(rute);
		return fuerKind;
	}
}
