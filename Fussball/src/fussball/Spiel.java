package fussball;

import java.util.Date;

public class Spiel {
	private Mannschaft unsereMannschaft;
	private boolean heimpiel;
	private int unsereTore, gegnerTore;
	private Date datum;
	private Spieler[] gelbeKarten,roteKarten;
	public Spiel(Mannschaft unsereMannschaft, boolean heimpiel, int unsereTore, int gegnerTore, Date datum,
			Spieler[] gelbeKarten, Spieler[] roteKarten) {
		super();
		this.unsereMannschaft = unsereMannschaft;
		this.heimpiel = heimpiel;
		this.unsereTore = unsereTore;
		this.gegnerTore = gegnerTore;
		this.datum = datum;
		this.gelbeKarten = gelbeKarten;
		this.roteKarten = roteKarten;
	}
	public Mannschaft getUnsereMannschaft() {
		return unsereMannschaft;
	}
	public void setUnsereMannschaft(Mannschaft unsereMannschaft) {
		this.unsereMannschaft = unsereMannschaft;
	}
	public boolean isHeimpiel() {
		return heimpiel;
	}
	public void setHeimpiel(boolean heimpiel) {
		this.heimpiel = heimpiel;
	}
	public int getUnsereTore() {
		return unsereTore;
	}
	public void setUnsereTore(int unsereTore) {
		this.unsereTore = unsereTore;
	}
	public int getGegnerTore() {
		return gegnerTore;
	}
	public void setGegnerTore(int gegnerTore) {
		this.gegnerTore = gegnerTore;
	}
	public Date getDatum() {
		return datum;
	}
	public void setDatum(Date datum) {
		this.datum = datum;
	}
	public Spieler[] getGelbeKarten() {
		return gelbeKarten;
	}
	public void setGelbeKarten(Spieler[] gelbeKarten) {
		this.gelbeKarten = gelbeKarten;
	}
	public Spieler[] getRoteKarten() {
		return roteKarten;
	}
	public void setRoteKarten(Spieler[] roteKarten) {
		this.roteKarten = roteKarten;
	}
}
