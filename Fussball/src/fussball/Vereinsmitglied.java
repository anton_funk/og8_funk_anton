package fussball;

public class Vereinsmitglied {
	private String name;
	private long telefonnummer;
	private boolean beitragGezahlt;
	public Vereinsmitglied(String name, long telefonnummer) {
		super();
		this.name = name;
		this.telefonnummer = telefonnummer;
		this.beitragGezahlt = false;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getTelefonnummer() {
		return telefonnummer;
	}
	public void setTelefonnummer(long telefonnummer) {
		this.telefonnummer = telefonnummer;
	}
	public boolean isBeitragGezahlt() {
		return beitragGezahlt;
	}
	public void setBeitragGezahlt(boolean beitragGezahlt) {
		this.beitragGezahlt = beitragGezahlt;
	}
	
}
