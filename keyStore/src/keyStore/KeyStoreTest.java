package keyStore;
import java.util.Scanner;

/* KeyManagementTest.java
 In der Klasse KeyStoreTest wird die Klasse KeyStore01 erprobt.
 AUFGABE:
 1. Implementieren Sie die Klasse KeyStore mit Hilfe eines Arrays

 @author  OSZ IMT
 @version 2.2   Datum: 28.11.2016
 */

public class KeyStoreTest {

	static void menue() {
		System.out.println("\n ***** Key-Verwaltung *******");
		System.out.println(" 1) eintragen ");
		System.out.println(" 2) finden ");
		System.out.println(" 3) l�schen");
		System.out.println(" 4) zeigen");
		System.out.println(" 5) Beenden");
		System.out.println(" 6) f�llen");//f�r besseres testen
		System.out.println(" ********************");
		System.out.print(" Bitte die Auswahl treffen: ");
	} // menue

	public static void main(String args[]) {
		Scanner myScanner = new Scanner(System.in);
		KeyStore03 aKeyStore = new KeyStore03();
		char wahl;
		String eintrag;
		
		int index;
		do {
			menue();
			wahl = myScanner.next().charAt(0);
			switch (wahl) {
			case '1':
				System.out.print("\nSchl�ssel: ");
				eintrag = myScanner.next();
				if (aKeyStore.add(eintrag))
					System.out.println("Schl�ssel wurde eingetragen\n");
				else
					System.out.println("KeyStore voll \n");
				break;
			case '2':
				System.out.print("\nGesuchter Schl�ssel: ");
				eintrag = myScanner.next();
				index = aKeyStore.indexOf(eintrag);
				if (index >= 0)
					System.out.println("Eintrag wurde an " + index + ". Stelle gefunden\n");
				else
					System.out.println("Eintrag nicht gefunden\n");
				break;
			case '3':
				System.out.print("\nZu loeschender Eintrag: ");
				eintrag = myScanner.next();
				index = aKeyStore.indexOf(eintrag);
				if (index >= 0) {
					aKeyStore.remove(index);
					System.out.println("Eintrag wurde geloescht\n");
				} else
					System.out.println("Eintrag nicht gefunden\n");
				break;
			case '4':
				System.out.println("\n  "+aKeyStore);
				break;
			case '5':
				System.exit(0);
				break;
			case '6':
				System.out.println("Anzahl einzutragender Zahlen: ");
				int anzahl = myScanner.nextInt();
				for(int i=0;i<anzahl;i++)
					aKeyStore.add(i+"");
				break;
			default:
				menue();
				wahl = myScanner.next().charAt(0);
			} // switch

		} while (wahl != 5);
	}// main
}// ZahlenspeicherTest

