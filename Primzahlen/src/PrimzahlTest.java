import java.util.Scanner;

public class PrimzahlTest {

	public static void main(String[] args) {
		Scanner scan=new Scanner(System.in);
		Stoppuhr uhr= new Stoppuhr();
		System.out.println("Zahl eingeben:");
		long l=scan.nextLong();
		uhr.start();
		boolean isPrimzahl=Primzahl.isPrimzahl(l);
		uhr.stopp();
		System.out.println(isPrimzahl);
		System.out.println("Zeit: "+uhr.getDauerInMs());
		scan.close();
	}

}
