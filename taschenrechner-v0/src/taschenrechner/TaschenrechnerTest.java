package taschenrechner;

import java.util.Scanner;

public class TaschenrechnerTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scan = new Scanner(System.in);
		Taschenrechner ts = new Taschenrechner();

		int swValue;

		// Display menu graphics
		System.out.println("============================");
		System.out.println("|   MENU SELECTION DEMO    |");
		System.out.println("============================");
		System.out.println("| Options:                 |");
		System.out.println("|        1. Addieren       |");
		System.out.println("|        2. Subtrahieren   |");
		System.out.println("|        3. Multiplizieren |");
		System.out.println("|        4. Dividieren     |");
		System.out.println("============================");
		System.out.print(" Select option: ");
		swValue = scan.next().charAt(0);

		// Switch construct
		scan.nextInt();
		switch (swValue) {
		case '1':
			System.out.println(ts.add(scan.nextInt(), scan.nextInt()));
			break;
		case '2':
			System.out.println(ts.sub(scan.nextInt(), scan.nextInt()));
			break;
		case '3':
			System.out.println(ts.mul(scan.nextInt(), scan.nextInt()));
			break;
		case '4':
			System.out.println(ts.div(scan.nextInt(), scan.nextInt()));
			break;
		case '5':
			break;

		// add your code here

		default:
			System.out.println("Invalid selection");
			break; // This break is not really necessary
		}
		scan.close();

	}

}